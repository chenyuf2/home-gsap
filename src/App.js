import "./App.scss";
import arrow from "./images/arrow-right.svg";
import imgGirl from "./images/girl.webp";
import imgBoy from "./images/boy.webp";
import React, { useRef, useEffect } from "react";
import { Power3 } from "gsap";
import { Tween } from "react-gsap";
const App = () => {
  let images = useRef(null);
  // let app = useRef(null);
  // let content = useRef(null);
  // let tl = new TimelineLite({ delay: 0.8 });

  return (
    <div className="hero">
      <div className="container">
        <div className="hero-inner">
          <div className="hero-content">
            <div className="hero-content-inner">
              <h1>
                <Tween
                  from={{
                    opacity: 0,
                    y: 20,
                    ease: Power3.easeOut,
                    delay: 0.95,
                  }}
                  stagger={0.15}
                >
                  <div className="hero-content-line">
                    <div className="hero-content-line-inner">
                      Relieving the burden
                    </div>
                  </div>

                  <div className="hero-content-line">
                    <div className="hero-content-line-inner">
                      of disease caused
                    </div>
                  </div>
                  <div className="hero-content-line">
                    <div className="hero-content-line-inner">by behavior.</div>
                  </div>
                </Tween>
              </h1>
              <Tween
                from={{
                  opacity: 0,
                  y: 20,
                  ease: Power3.easeOut,
                  delay: 1.5,
                }}
              >
                <p>
                  Better treats serious cardiometabolic diseases to transform
                  lives and reduce healthcare utilization through the use of
                  digital therapeutics.
                </p>
              </Tween>
              <Tween
                from={{
                  opacity: 0,
                  y: 20,
                  ease: Power3.easeOut,
                  delay: 1.7,
                }}
              >
                <div className="btn-row">
                  <button className="explore-button">
                    Explore
                    <div className="arrow-icon">
                      <img src={arrow} alt="row" />
                    </div>
                  </button>
                </div>
              </Tween>
            </div>
          </div>
          <div className="hero-images">
            <div ref={(el) => (images = el)} className="hero-images-inner">
              <Tween
                from={{
                  opacity: 0,
                  y: 1280,
                  ease: Power3.easeOut,
                }}
              >
                <div className="hero-image girl">
                  <Tween
                    from={{
                      scale: 1.6,
                      ease: Power3.easeOut,
                      delay: 0.1,
                    }}
                  >
                    <img src={imgGirl} alt="girl" />
                  </Tween>
                </div>
              </Tween>
              <Tween
                from={{
                  opacity: 0,
                  y: 1280,
                  scale: 1.6,
                  ease: Power3.easeOut,
                  delay: 0.3,
                }}
              >
                <div className="hero-image boy">
                  <Tween
                    from={{
                      scale: 1.6,
                      ease: Power3.easeOut,
                      delay: 0.4,
                    }}
                  >
                    <img src={imgBoy} alt="boy" />
                  </Tween>
                </div>
              </Tween>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
